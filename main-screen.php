<?php
session_start();

if (isset($_POST['syncid'])) {
	$_SESSION['syncid'] = $_POST['syncid'];
}
?>

<div class="wrap">
	<h2>Master Sync</h2>

	<?php
	if(isset($_POST['btntest'])){
	}

	include 'progBar_php.php';

	if (isset($_GET['startUploads'])) {
		if(isset($_GET['url'])){
			$path = $paths[$_GET['url']];
			$username = $usernames[$_GET['url']];
			$host = dbs_stripHttp($_GET['url']);
			syncUploads($username, $host, $path);

			ob_start();
			dbs_mysqldump();
			$sql = ob_get_clean();
			$report = dbs_post($_GET['url'], 'dbs_push', array(
				'secret' => $tokens[$_GET['url']],
				'sql' => $sql
				));

			print_r($report);
		}
		?>
		<script type="text/javascript">
		// startTask();
		</script>
		<?php
	}
	$sites; 
	$string = $_SERVER['QUERY_STRING'];

	$new = explode('&', $string);

	for($i=0; $i<count($new); $i++) {	
		list($key, $value) = explode("=", $new[$i]);

		if (strcasecmp($key, 'message') == 0){
			$message[] = $value;
		}
		elseif (strcasecmp($key, 'error') == 0){
			$error[] = $value ;
		}
	}
	if (!empty($error)) {
		$text = $error[count($error)-1]; 
		$text = str_replace('+', ' ', $text); 
		$text1 = substr($text, 0, strpos($text, '&'));

		if(count($error) == 1){
			echo '<div class="error"><p>' .str_replace('+', ' ', $error[0]). '</p></div>';
		}else{
			for($i=0; $i<count($error); $i++) {

				if($i == count($error)-1) {
					echo '<div class="error"><p>' . $text1 . '</p><br></div>';
				}else {
					echo '<div class="error"><p>' . str_replace('+', ' ', $error[$i]) . '</p></div>';
				}
			}
		}
	} 

	if (!empty($message)) {
		$text = $message[count($message)-1]; 
		$text = str_replace('+', ' ', $text); 
		$text1 = substr($text, 0, strpos($text, '&'));
		if(count($message) == 1){
			echo '<div class="updated"><p>' . str_replace('+', ' ', $message[0]).'</p></div>';
		}else{
			for($i=0; $i<count($message); $i++) {

				if($i == count($message)-1){
					echo '<div class="updated"><p>' . $text1 . '</p></div>';
				}else {
					echo '<div class="updated"><p>' . str_replace('+', ' ', $message[$i]) . '</p></div>';
				}
			}
		}
	} 
	?>

	<form method="post" action="<?php dbs_url(); ?>">
		<input type="hidden" name="dbs_action" value="sync_all">
		<p>
			<input type="submit" value="Sync All Linked Sites" class="button-primary">
		</p>
		<!-- <input type="hidden" name="dbs_action" value="test">
		<p>
			<input type="submit" value="Test" class="button-primary">
		</p> -->
	</form>
	<!-- <form method="post" action="">
		<p>
			<input type="button" onclick="startTask();" value="Start Task" class="button-primary">
		</p>
	</form> -->

	<table class="form-table">
		<tr valign="top">
			<th scope="row">My token</th>
			<td>
				<?php echo dbs_getToken(); ?>
				<div>Keep this secret. Anyone with this token can gain access to your database.</div>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">Linked sites</th>
			<td>
				<?php if ($tokens && $paths && $usernames): $i = 0;?>
				<?php foreach ($tokens as $url => $secret): ?>
				<?php
				$path = $paths[$url];
				$username = $usernames[$url];
				$id = $i;
				?>

				<form method="post" action="">
					<input type="hidden" name="dbs_action" value="remove">
					<input type="hidden" name="url" value="<?php echo $url; ?>">
					<p>
						<a href="<?php echo $url; ?>/wp-admin/tools.php?page=dbs_options"><?php echo dbs_stripHttp($url); ?></a>
						<input type="submit" value="Remove" class="button-secondary">
					</p>
				</form>
				<form method="post" action="<?php dbs_url(); ?>">
					<input type="hidden" name="dbs_action" value="push">
					<input type="hidden" name="url" value="<?php echo $url; ?>">
					<p>
						<b>Delete all data</b> in the remote WordPress database and replace with the data from this database.
						<input type="submit" value="Sync" class="button-primary">
					</p>
				</form>
				<!-- <form method="post" action="">
					<input type="hidden" name="host" value="<?php echo dbs_stripHttp($url); $hosts[$url] = dbs_stripHttp($url); ?>">
					<input type="hidden" name="path" value="<?php echo $path; $paths[$url] = $path; ?>">
					<input type="hidden" name="url" value="<?php echo $url; $urls[$id] = $url; ?>">
					<input type="hidden" name="username" value="<?php echo $username; $usernames[$url] =$username; ?>">
					<p>
						<b>Sync the Uploads folder</b> in the remote WordPress database and this database.
						<input type="button" onclick="toPost(<?php echo $id; ?>); startTask();" value="Upload" class="button-primary">
					</p>
				</form> -->
				<?php
				$sites = array($url => $secret); 

				$i = $i+1;
				endforeach; 
				$_SESSION['synchosts'] = $hosts;
				$_SESSION['syncusernames'] = $usernames;
				$_SESSION['syncurls'] = $urls;
				$_SESSION['syncpaths'] = $paths;
				?>
			<?php else: ?>
			No sites linked yet
		<?php endif; ?>
		<?php
		function getSites() {
			$linked_sites = $sites;

			return $linked_sites;
		}
		?>
	</td>
</tr>
<tr valign="top">
	<th scope="row">Add a remote site token</th>
	<td>
		<form method="post" action="">
			<input type="hidden" name="dbs_action" value="add">
			<p> Enter Site Token: </p>
			<input type="input" size="60" name="token" class="regular-text">
			<p> Enter SSH Username: </p>
			<input type="input" size="60" name="username" value="www-data" class="regular-text">
			<p> Enter Full Path to Website Folder: </p>
			<input type="input" size="60" name="webfolder" value="/var/www/" class="regular-text"> <br><br>
			<input type="submit" value="Add" class="button-primary">
		</form>
	</td>
</tr>
<tr valign="top">
	<th scope="row">Dump database manually</th>
	<td>
		<a href="<?php echo admin_url('admin-ajax.php?action=dbs_pull&dump=manual'); ?>" target="_blank" class="button">Dump</a>
	</td>
</tr>
</table>