<?php
session_start();
/*
Plugin Name: Master Sync
Description: Sync databases across servers with a single click.
Version: 1.0
Author: allsolve
*/

//What WordPress capability is required to access this plugin?
define('DBS_REQUIRED_CAPABILITY', 'manage_options');

//API version for forward-compatibility
define('DBS_API_VERSION', 1);

require_once 'functions.php';

//add a menu item under Tools
add_action('admin_menu', 'dbs_menu');

wp_register_style ( 'mystyle1', 'http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' );
wp_register_style ( 'mystyle2', 'http://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css' );
wp_register_script ( 'progBar1', 'https://code.jquery.com/jquery-1.12.4.js' );
wp_register_script ( 'progBar2', 'https://code.jquery.com/ui/1.12.0/jquery-ui.js' );

function dbs_menu() {
	add_submenu_page('tools.php', 'Master Sync', 'Master Sync', DBS_REQUIRED_CAPABILITY, 'dbs_options', 'dbs_admin_ui');
}

//display admin menu page
function dbs_admin_ui() {
	if (!current_user_can('manage_options')) {
		wp_die(__('You do not have sufficient permissions to access this page.'));
	}

	$action = isset($_REQUEST['dbs_action']) ? $_REQUEST['dbs_action'] : 'index';
	switch ($action) {
		default :
			$usernames = get_option('outlandish_sync_usernames') ? : array();
			$paths = get_option('outlandish_sync_paths') ? : array();
			$tokens = get_option('outlandish_sync_tokens') ? : array();
			include 'main-screen.php';
			break;
	}

	wp_enqueue_style('mystyle1');
	wp_enqueue_style('mystyle2');
	wp_enqueue_script('progBar1');
	wp_enqueue_script('progBar2');
}

//do most actions on admin_init so that we can redirect before any content is output
add_action('admin_init', 'dbs_post_actions');
function dbs_post_actions() {
	if (!isset($_REQUEST['dbs_action'])) return;

	if (!current_user_can(DBS_REQUIRED_CAPABILITY)) {
		wp_die(__('You do not have sufficient permissions to access this page.'));
	}

	$tokens = get_option('outlandish_sync_tokens') ? : array();
	$usernames = get_option('outlandish_sync_usernames') ? : array();
	$paths = get_option('outlandish_sync_paths') ? : array();

	switch ($_REQUEST['dbs_action']) {
		//add a token
		case 'add' :
			$decodedToken = base64_decode($_POST['token']);
			@list($secret, $url) = explode(' ', $decodedToken);
			if (empty($secret) || empty($url) || !preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url)) {
				$gotoUrl = dbs_url(array('error' => 'The token is not valid.'));
			} elseif ($url == get_bloginfo('wpurl')) {
				$gotoUrl = dbs_url(array('error' => 'The token cannot be added as it is for this installation.'));
			} else {
				$tokens[$url] = $secret;
				update_option('outlandish_sync_tokens', $tokens);
				//get the username from the form and insert it into the usernames array
				$usernames[$url] = trim($_POST['username']);
				update_option('outlandish_sync_usernames', $usernames);
				$paths[$url] = trim($_POST['webfolder']);
				update_option('outlandish_sync_paths', $paths);
				$gotoUrl = dbs_url();
			}
			wp_redirect($gotoUrl);
			exit;

		//remove a token
		case 'remove' :
			unset($tokens[$_POST['url']]);
			update_option('outlandish_sync_tokens', $tokens);
			wp_redirect(dbs_url());
			exit;

		//pull from remote db
		case 'pull' :
			try {
				//send post request with secret
				$result = dbs_post($_REQUEST['url'], 'dbs_pull', array('secret' => $tokens[$_REQUEST['url']]));
				if ($result == 'You don\'t know me') {
					$gotoUrl = dbs_url(array('error' => 'Invalid site token'));
				} elseif ($result == '0') {
					$gotoUrl = dbs_url(array('error' => 'Sync failed. Is the plugin activated on the remote server?'));
				} else {

					$sql = $result;
					if ($sql && preg_match('|^/\* Dump of database |', $sql)) {

						//backup current database
						$backupfile = dbs_makeBackup();

						//store some options to restore after sync
						$optionCache = dbs_cacheOptions();

						//load the new data
						if (dbs_loadSql($sql)) {
							//clear object cache
							wp_cache_flush();

							//restore options
							dbs_restoreOptions($optionCache);
							$gotoUrl = dbs_url(array('message' => 'Database synced successfully'));
						} else {
							//import failed part way through so attempt to restore last backup
							$compressedSql = substr(file_get_contents($backupfile), 10); 
							//strip gzip header
							dbs_loadSql(gzinflate($compressedSql));

							$gotoUrl = dbs_url(array('error' => 'Sync failed. SQL error.'));
						}
					} else {
						$gotoUrl = dbs_url(array('error' => 'Sync failed. Invalid dump.'));
					}
				}
			} catch (Exception $ex) {
				$gotoUrl = dbs_url(array('error' => 'Remote site not accessible (HTTP ' . $ex->getCode() . ')'));
			}

			wp_redirect($gotoUrl);
			exit;

		//push to remote db
		case 'push' :
			//get SQL data
			// ob_start();
			// dbs_mysqldump();
			// $sql = ob_get_clean();
			// try {

				//Get Username and host from the usernames array
				// $username = $usernames[$_REQUEST['url']];
				// $host = dbs_stripHttp($_REQUEST['url']);
				// $path = $paths[$_REQUEST['url']];

				// syncUploads($username, $host, $path);

				//send post request with secret and SQL data
				// $result = dbs_post($_REQUEST['url'], 'dbs_push', array(
				// 	'secret' => $tokens[$_REQUEST['url']],
				// 	'sql' => $sql
				// ));

				// if ($result == 'You don\'t know me') {
				// 	//$gotoUrl = dbs_url(array('error' => 'Invalid site token'));
				// 	$errors = 'error=Invalid+site+token+on+website+'.$host;
				// } elseif ($result == '0') {
				// 	//$gotoUrl = dbs_url(array('error' => 'Sync failed. Is the plugin activated on the remote server?'));
				// 	$errors = 'error=Sync+failed.+Is+the+plugin+activated+on+the+remote+server?+on+website+'.$host;
				// } elseif ($result == 'OK') {
				// 	//$gotoUrl = dbs_url(array('message' => 'All Databases synced successfully'));
				// 	//syncUploads($username, $host, $path);
				// 	//$succesful[] ='host='.$host.'&username='.$username.'&path='.$path;
				// 	$_SESSION['synchost'] = $host;
				// 	$_SESSION['syncpath'] = $path;
				// 	$_SESSION['syncusername'] = $username;
				// 	$status = 'message=Database+Synced+successfully+on+website+'.$host;
				// } else {
				// 	$errors = 'error=Something+may+be+wrong+on+website+'.$host;
				// 	//$gotoUrl = dbs_url(array('error' => 'Something may be wrong'));
				// }

				// if(!empty($errors) && !empty($status)){
				// 	$params = $errors. '&' .$status. '&startUploads=OK;&page=dbs_options';
				// 	// $syncUrl = implode(';', $succesful);
				// }elseif(empty($errors) && !empty($status)){
					$params = $status . '&startUploads=OK&page=dbs_options&url='.$_REQUEST['url'];
					// $syncUrl = implode(';', $succesful);
			// 	}elseif(!empty($errors) && empty($status)){
			// 		$params = $errors . '&page=dbs_options';
			// 	}else {
			// 		$params = 'No+Database+was+synced&page=dbs_options';
			// 	}
			// } catch (RuntimeException $ex) {
			// 	$params = 'error=Remote+site+not+accessible+(HTTP+' .$ex->getCode(). '&page=dbs_options';
			// }
			wp_redirect(admin_url('tools.php?'.$params));
			exit;

		//Sync All
		case 'sync_all' :
			//get SQL data
			ob_start();
			dbs_mysqldump();
			$sql = ob_get_clean();
			
			try {				
				foreach ($tokens as $url => $secret) :
				//Get Username and host from the usernames array
				$username = $usernames[$url];
				$host = dbs_stripHttp($url);
				$path = $paths[$url];

				//send post request with secret and SQL data
				$result = dbs_post($url, 'dbs_push', array(
					'secret' => $secret,
					'sql' => $sql
				));
				if ($result == 'You don\'t know me') {
					//$gotoUrl = dbs_url(array('error' => 'Invalid site token'));
					$errors[] = 'error=Invalid+site+token+on+website+'.$host;
				} elseif ($result == '0') {
					//$gotoUrl = dbs_url(array('error' => 'Sync failed. Is the plugin activated on the remote server?'));
					$errors[] = 'error=Sync+failed.+Is+the+plugin+activated+on+the+remote+server?+on+website+'.$host;
				} elseif ($result == 'OK') {
					//$gotoUrl = dbs_url(array('message' => 'All Databases synced successfully'));
					//syncUploads($username, $host, $path);
					$succesful[] ='host='.$host.'&username='.$username.'&path='.$path;
					$status[] = 'message=Database+Synced+successfully+on+website+'.$host;
				} else {
					$errors[] = 'error=Something+may+be+wrong+on+website+'.$host;
					//$gotoUrl = dbs_url(array('error' => 'Something may be wrong'));
				}
				endforeach;

				if(!empty($errors) && !empty($status)){
					$params = implode(';', $errors) .';'. implode(';', $status).'&startUploads=OK;&page=dbs_options';
					$syncUrl = implode(';', $succesful);
				}elseif(empty($errors) && !empty($status)){
					$params = implode(';', $status). '&startUploads=OK;&page=dbs_options';
					$syncUrl = implode(';', $succesful);
				}elseif(!empty($errors) && empty($status)){
					$params = implode(';', $errors). '&page=dbs_options';
				}else {
					$params = 'No+Database+was+synced&page=dbs_options';
				}
			} catch (RuntimeException $ex) {
				$gotoUrl = dbs_url(array('error' => 'Remote site not accessible (HTTP ' . $ex->getCode() . ')'));
			}
			$_SESSION['syncUrl'] = $syncUrl;
			wp_redirect(admin_url('tools.php?'.$params));
			exit;

			case 'test' :
			//$gotoUrl = dbs_url(array('message' => 'All Databases synced successfully', 'error' => 'You fvcked up', 'message' => 'All Databases synced successfully', 'error' => 'You fvcked up'));

			// $stats = passthru('rsync -a --info=progress2 /var/www/testport/multisite/siteB/wp-content/uploads/ root@192.168.1.50:/tmp/00Test/');
			// echo $stats;

			//wp_redirect($gotoUrl);

			//wp_redirect(admin_url('tools.php?message=Sync+failed.+Is+the+plugin+activated+on+the+remote+server?+on+website+http://192.168.1.50/multisite/siteD;message=Database+Synced+successfully+on+website+http://192.168.1.50/multisite/siteB;message=Database+Synced+successfully+on+website+http://192.168.1.50/multisite/siteC&page=dbs_option'));

			//wp_redirect(admin_url('tools.php?message=All+Databases+synced+successfully;error=All+Databases+synced+successfully;message=All+Databases+synced+successfully&page=dbs_options'));
			exit;
	}
}

//handle remote pull action when not logged in
add_action('wp_ajax_nopriv_dbs_pull', 'dbs_pull_nopriv');
function dbs_pull_nopriv() {
	//test for secret
	$secret = dbs_getSecret();
	if (stripslashes($_REQUEST['secret']) != $secret) {
		die("You don't know me");
	}
	//dump DB
	dbs_pull();
}

//handle pull action when logged in
add_action('wp_ajax_dbs_pull', 'dbs_pull');
function dbs_pull() {
	//dump DB and GZip it
	header('Content-type: application/octet-stream');
	if(isset($_GET['dump'])){
		if ($_GET['dump'] == 'manual') {
			//manual dump, so include attachment headers
			header('Content-Description: File Transfer');
	        header('Content-Disposition: attachment; filename=data.sql');
	        header('Content-Transfer-Encoding: binary');
	        header('Expires: 0');
	        header('Cache-Control: must-revalidate');
	        header('Pragma: public');
		}
	}
	dbs_mysqldump();
	exit;
}

//handle remote push action
add_action('wp_ajax_nopriv_dbs_push', 'dbs_push');
function dbs_push() {
	//test for secret
	$secret = dbs_getSecret();
	if (stripslashes($_REQUEST['secret']) != $secret) {
		die("You don't know me");
	}


	$tokens = get_option('outlandish_sync_tokens') ? : array();

//	echo $sql = gzinflate($_POST['sql']);
	$sql = stripslashes($_POST['sql']);
	if ($sql && preg_match('|^/\* Dump of database |', $sql)) {

		//backup current DB
		dbs_makeBackup();

		//store options
		$optionCache = dbs_cacheOptions();

		//load posted data
		dbs_loadSql($sql);

		//clear object cache
		wp_cache_flush();

		//reinstate options
		dbs_restoreOptions($optionCache);

		echo 'OK';
	} else {
		echo 'Error: invalid SQL dump';
	}
	exit;
	
}

/**
 * @return array key-value pairs of selected current WordPress options
 */
function dbs_cacheOptions() {
	//persist these options
	$defaultOptions = array('siteurl', 'home', 'outlandish_sync_tokens', 'outlandish_sync_secret', 'outlandish_sync_usernames', 'outlandish_sync_paths');
	$persistOptions = apply_filters('dbs_persist_options', $defaultOptions);

	$optionCache = array();
	foreach ($persistOptions as $name) {
		$optionCache[$name] = get_option($name);
	}
	return $optionCache;
}

/**
 * @param array $optionCache key-value pairs of options to restore
 */
function dbs_restoreOptions($optionCache) {
	foreach ($optionCache as $name => $value) {
		update_option($name, $value);
	}
}