<?php
session_start();
header('Content-Type: text/event-stream');
// recommended to prevent caching of event data.
header('Cache-Control: no-cache'); 

require_once 'functions.php';

// if (isset($_POST['syncid'])) {
//     $_SESSION['syncid'] = $_POST['syncid'];
// }

function send_message($id, $message) {
    $d = array('message' => $message);

    echo "id: $id" . PHP_EOL;
    echo "data: " . json_encode($d) . PHP_EOL;
    echo PHP_EOL;

    ob_flush();
    flush();
}
  
//LONG RUNNING TASK
//for($i = 1; $i <= 10; $i++) {
//    send_message($i, 'on iteration ' . $i . ' of 10' , $i*10); 
// 
//    sleep(1);
//}
//  
//send_message('CLOSE', 'Process complete');

if(isset($_GET['host'])){

    $string = $_SERVER['QUERY_STRING'];

    echo $string;

    $new = explode('&', $string);

    print_r($new);

    for ($k=0; $k<count($new); $k++){
        $params = explode('&', $new[$k]);
    }

    print_r($params);

    for($i=0; $i<count($params); $i++) {
        list($key, $value) = explode("=", $params[$i]);
        
        if (strcasecmp($key, 'path') == 0){
            $paths[] = $value;
        } elseif (strcasecmp($key, 'username') == 0){
            $usernames[] = $value ;
        } elseif (strcasecmp($key, 'host') == 0){
            $hosts[] = $value ;
        }
    }

    print_r($paths);
    print_r($usernames);
    print_r($hosts);

    if($usernames && $paths && $hosts) {

        foreach ($usernames as $key => $value) {
            $username = $usernames[$key];
            $path = $paths[$key];
            $host = $hosts[$key];
            $stripped_host = substr($host, 0, strpos($host, "/"));
            send_message('Start', 'Upload for ' .$host. ' Started');
            send_message('Done', 'Upload for' .$host. 'complete');
        }

        send_message('CLOSE', 'All Uploads completed successfully');
    }
} elseif(isset($_SESSION['syncUrl'])) {

    $string = $_SESSION['syncUrl'];
    $new = explode(';', $string);
    print_r($new);

    for ($k=0; $k<count($new); $k++){
        $params[] = explode('&', $new[$k]);
    }

    print_r($params);

    foreach($params as $index => $data){
        $arr = $data;
        for($i=0; $i<count($data); $i++) {
            list($key, $value) = explode("=", $data[$i]);
            
            if (strcasecmp($key, 'path') == 0){
                $paths[] = $value;
            } elseif (strcasecmp($key, 'username') == 0){
                $usernames[] = $value ;
            } elseif (strcasecmp($key, 'host') == 0){
                $hosts[] = $value ;
            }
        }
    }

    print_r($paths);
    print_r($usernames);
    print_r($hosts);

    if($usernames && $paths && $hosts) {

        foreach ($usernames as $key => $value) {

            $host = $hosts[$key];
            send_message('Start', 'Upload for ' .$host. ' Started');
            send_message('DoneOne', '<div class="updated"><p>Upload for' .$host. 'complete</p></div>');
            sleep(5);
            send_message('Done', '<div class="updated"><p>next upload starting......</p></div>');
            sleep(5);
        }

        unset($_SESSION['syncUrl']);
        send_message('CLOSE', '<div class="updated"><p>All Uploads completed successfully</p></div>');
    }
    //unset($_SESSION['syncurl']);
} else if(isset($_SESSION['syncpath']) && isset($_SESSION['synchost']) && isset($_SESSION['syncusername'])) {

    $username = $_SESSION['syncusername'];
    $path = $_SESSION['syncpath'];
    $host = $_SESSION['synchost'];
    $stripped_host = substr($host, 0, strpos($host, "/"));

    send_message('Start', '<div class="updated"><p>Upload for <b>' .$host. '</b> Started. fp='.$fp.'</p></div>');

    send_message('DoneOne', '<div class="updated"><p>Upload for <b>' .$host. '</b> complete</p></div>');
    sleep(5);

    send_message('CLOSE', '<div class="updated"><p>All Uploads completed successfully</p></div>');
    unset ($_SESSION['syncusername']);
    unset ($_SESSION['syncpath']);
    unset ($_SESSION['synchost']);
    unset ($stripped_host);
    unset($_SESSION['syncid']);
} else { 
    send_message('CLOSE', '<div class="error"><p>Fatal Error: Nothing is sent to the page. /sadface/</p></div>');}


?>