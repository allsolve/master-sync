<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script>
    var es;
    function startTask() {
        <?php
        $wp_progressBar = plugins_url( 'wp_progressBar.php', __FILE__ );
        ?>

        es = new EventSource('<?php echo $wp_progressBar; ?>');

        // a message is received
        es.addEventListener('message', function(e) {
            var result = JSON.parse( e.data );

            // addLog(result.message);

            var probBar = document.getElementsByClassName("progress-bar")[0];
            var currentStatus = document.getElementsByClassName("curr")[0];
            // $(".progress-bar").css("width", result.message);

            if(e.lastEventId == 'CLOSE') {
                // addLog('Received CLOSE closing');
                es.close();
                probBar.style.width = "100%";
                currentStatus.innerHTML = result.message;
                
                // var pBar = document.getElementById('progressor');
                //max out the progress bar
                // pBar.value = pBar.max;   
            } else if(e.lastEventId == 'Start') {
                // $(".progress-bar").css("width", "0%");
                // $(".curr").html(result.message);
                currentStatus.innerHTML = result.message;
            } else if(e.lastEventId == 'Done') {
                // $(".prog").append('<div id="progress-bar" class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"> <br> <span class="sr-only">70% Complete</span>');
                //$(".curr").html(result.message);

                // var proG = document.getElementsByClassName("prog")[0];
                // proG.innerHTML = '<div class="progress"> <br> <div id="progress-bar" class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"> <br> <span class="sr-only">70% Complete</span> <br> </div> <br> <div>';

                currentStatus.innerHTML = result.message;

                //$(".progress-bar").css("width", "0%");
                probBar.style.width = "0%";
            } else if(e.lastEventId == 'DoneOne') {
                currentStatus.innerHTML = result.message;
                probBar.style.width = "100%";
            } 
            else {

                // var pb = document.getElementById('progress-bar');
                // pb.css("width", result.message); 
                console.log("HERE!");
                // var pBar = document.getElementById('progressor');
                // pBar.value = result.progress;
                // var perc = document.getElementById('percentage');
                // perc.innerHTML   = result.progress;
                // perc.style.width = (Math.floor(pBar.clientWidth * (result.progress/100)) + 15) + 'px';
            }
        });

es.addEventListener('error', function(e) {
    // addLog('Error occurred');
    es.close();
});
}

function stopTask() {
    es.close();
//  addLog('Interrupted');
}

function addLog(message) {
    var r = document.getElementById('results');
    r.innerHTML += message + '<br>';
    r.scrollTop = r.scrollHeight;

// $(".progress-bar").css("width", message); 
}
</script>
</head>
<body>
<!-- <div id="results" style="border:1px solid #000; padding:10px; width:300px; height:250px; overflow:auto; background:#eee;"></div>
    <br /> -->

    <div class="curr"></div>

    <div class="prog"></div>

    <div class="progress">
        <div id="progress-bar" class="progress-bar" role="progressbar" aria-valuenow="70"
        aria-valuemin="0" aria-valuemax="100">
        <span class="sr-only">70% Complete</span>
    </div>
</div> <br>
</body>
</html>